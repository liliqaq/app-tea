package com.tea;

import com.tea.entity.UserEntity;
import com.tea.mapper.UserMapper;
import com.tea.repository.EsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author: tea
 * @date: 2020/12/15 20:21
 * @description:
 */
@SpringBootTest
public class Tests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private EsRepository esRepository;

    @Test
    public void findAll(){
        for (UserEntity userEntity : esRepository.findAll()) {
            System.out.println(userEntity);
        }
    }

}
