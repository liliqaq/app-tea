package com.tea.repository;

import com.tea.entity.UserEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author: tea
 * @date: 2020/12/15 20:24
 * @description:
 */
public interface EsRepository extends ElasticsearchRepository<UserEntity, Long> {

}
