package com.tea.mapper;

import com.tea.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: tea
 * @date: 2020/12/15 20:17
 * @description:
 */
@Mapper
public interface UserMapper {

    /**
     * findAll
     * @return
     */
    @Select("select * from user")
    List<UserEntity> findAll();

}
