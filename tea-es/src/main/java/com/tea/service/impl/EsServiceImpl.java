package com.tea.service.impl;

import com.tea.entity.ItemEntity;
import com.tea.entity.UserEntity;
import com.tea.mapper.UserMapper;
import com.tea.repository.EsRepository;
import com.tea.service.EsService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: tea
 * @date: 2020/12/15 20:23
 * @description:
 */
@Service
public class EsServiceImpl implements EsService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private EsRepository esRepository;

    @Override
    public void saveAll() {
        List<UserEntity> userEntityList = userMapper.findAll();
        esRepository.saveAll(userEntityList);
    }

    @Override
    public List<ItemEntity> getData() {
        // 关键字
        String key = "java";
        String url = "https://search.jd.com/Search?keyword=" + key;
        String html = sendGet(url);

        Document doc = Jsoup.parse(html);
        Elements imgNodeList = doc.select("img");
        Elements liNodeList = doc.select("li");

        List<ItemEntity> itemPojoList = new ArrayList<>();
        ItemEntity itemPojo = null;

        for (Element imgNode : imgNodeList) {
            String imgAddr = imgNode.attr("data-lazy-img");
            if (!StringUtils.isEmpty(imgAddr) && !imgAddr.equals("done")) {
                itemPojo = new ItemEntity();
                itemPojo.setImgAddr(imgAddr);
                itemPojo.setCreateTime(new Date());
                itemPojoList.add(itemPojo);
            }
        }

        List<String> titleList = new ArrayList<>();
        List<String> hrefList = new ArrayList<>();
        List<String> priceList = new ArrayList<>();
        for (Element li : liNodeList) {
            for (Element a : li.select("a")) {
                String title = a.attr("title");
                titleList.add(title);
                String href = a.attr("href");
                hrefList.add(href);
            }

            for (Element strong : li.select("strong")) {
                for (Element i : strong.select("i")) {
                    String price = i.text();
                    priceList.add("￥" + price);
                }
            }
        }

        for (int i = 0; i < itemPojoList.size(); i++) {
            itemPojoList.get(i).setTitle(titleList.get(i));
            if (i < priceList.size() ) {
                itemPojoList.get(i).setPrice(priceList.get(i));
            }
            itemPojoList.get(i).setUrl(hrefList.get(i));
        }

        return itemPojoList;
    }

    public String sendGet(String url) {
        String result = "";
        String urlName = url;
        try {
            URL realURL = new URL(urlName);
            URLConnection conn = realURL.openConnection();
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");
            conn.connect();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += "\n" + line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
