package com.tea.service;

import com.tea.entity.ItemEntity;

import java.util.List;

/**
 * @author: tea
 * @date: 2020/12/15 20:23
 * @description:
 */
public interface EsService {

    /**
     * saveAll
     */
    void saveAll();

    /**
     * 获取数据
     *
     * @return
     */
    List<ItemEntity> getData();
}
