package com.tea.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author：tea
 * @date: 2020/9/30
 * @description:
 */
@Data
public class ItemEntity implements Serializable {

    private Integer id;
    private String imgAddr;
    private String title;
    private String url;
    private String price;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    private Date createTime;

}