package com.tea.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @author: tea
 * @date: 2020/12/15 20:14
 * @description:
 */
@Data
@Document(indexName = "user")
public class UserEntity {

    @Id
    private Long id;

    @Field(value = "username", type = FieldType.Auto)
    private String username;

    @Field(value = "password", type = FieldType.Auto)
    private String password;

    @Field(value = "roleId", type = FieldType.Auto)
    private Long roleId;

}
