package com.tea.controller;

import com.tea.service.EsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author: tea
 * @date: 2020/12/15 20:22
 * @description:
 */
@Controller
public class EsController {

    @Autowired
    private EsService esService;

    @ResponseBody
    @GetMapping("/saveAll")
    void saveAll() {
        esService.saveAll();
    }


}
