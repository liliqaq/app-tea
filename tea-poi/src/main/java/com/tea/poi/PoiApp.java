package com.tea.poi;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.tea.poi.dto.ClauseImportRow;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.List;

/**
 * @author tea
 * @date 2022/11/18
 */
@Slf4j
public class PoiApp {

    public static void main(String[] args) throws Exception {
        File xls = ResourceUtils.getFile("classpath:202211161554014009.xls");
        // EasyExcel.read(xls, ClauseImportInfo.class, new ClauseListener()).doReadAll();

        ExcelReader excelReader = EasyExcel.read(xls).build();

        // ReadSheet readSheet1 =
        //         EasyExcel.readSheet(0).head(ClauseImportRow.class).registerReadListener(new ClauseListener()).headRowNumber(2).build();
        // // excelReader.read(readSheet1);
        //
        // ReadSheet readSheet2 =
        //         EasyExcel.readSheet(1).head(FreeClauseImportRow.class).registerReadListener(new ReadListener<FreeClauseImportRow>() {
        //             @Override
        //             public void invoke(FreeClauseImportRow data, AnalysisContext context) {
        //                 log.info(JSON.toJSONString(data));
        //             }
        //
        //             @Override
        //             public void doAfterAllAnalysed(AnalysisContext context) {
        //
        //             }
        //         }).headRowNumber(1).build();
        // excelReader.read(readSheet1,readSheet2);
        List<ClauseImportRow> objects1 = EasyExcelFactory.read(xls).sheet(0).head(ClauseImportRow.class).headRowNumber(2).doReadSync();
        System.out.println(objects1);
        // EasyExcel.read(xls, ClauseImportRow.class, new PageReadListener<ClauseImportRow>(dataList -> {
        //     for (ClauseImportRow demoData : dataList) {
        //         log.info("读取到一条数据{}", JSON.toJSONString(demoData));
        //     }
        // })).sheet(0).headRowNumber(2).doRead();

    }

}
