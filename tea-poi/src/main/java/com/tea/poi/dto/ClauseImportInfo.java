package com.tea.poi.dto;

import lombok.Data;

import java.util.List;

/**
 * 条款导入对象
 * @author zhoufeng
 */
@Data
public class ClauseImportInfo {

    private List<ClauseImportRow> clauseImportRows;


    private List<FreeClauseImportRow> freeClauseImportRows;


    private List<PumpClauseImportRow> pumpClauseImportRows;

}
