package com.tea.poi.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 免租期导入Row对象
 * @author zhoufeng
 */
@Data
@ExcelIgnoreUnannotated
public class FreeClauseImportRow {
    /**
     * 本行行号
     */
    private int rowNum;
    /**
     * 合同编号
     */
    @ExcelProperty("*合同编号")
    private String contractNo;
    /**
     * 甲方名称
     */
    private String firstName;
    /**
     * 乙方名称
     */
    private String secondName;
    /**
     * 免租期开始时间
     */
    private Long freeStartDate;

    /**
     * 免租期结束时间
     */
    private Long freeOverDate;

    public String getDateRangeStr(){
        return this.getFreeStartDate() + "-" + this.getFreeOverDate();
    }


}
