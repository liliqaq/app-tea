package com.tea.poi.dto;

import lombok.Data;

@Data
public class ImportResult  {
    private Integer resultId;

    private Integer batchId;

    private String batchNo;

    private Integer contractId;

    private String contractNo;

    private String checkField;

    private String checkFieldFmt;

    private String resultDesc;

    private Byte resultType;

    private Integer lineNum;


    private String instanceStr;

    private String toPdfStr;
    private static final long serialVersionUID = 1L;

}