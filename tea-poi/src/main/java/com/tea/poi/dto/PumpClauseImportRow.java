package com.tea.poi.dto;

import lombok.Data;

/**
 * 抽成条款导入Row对象
 * @author zhoufeng
 */
@Data
public class PumpClauseImportRow {
    /**
     * 本行行号
     */
    private int rowNum;
    /**
     * 合同编号
     */
    private String contractNo;
    /**
     * 甲方名称
     */
    private String firstName;
    /**
     * 乙方名称
     */
    private String secondName;

    /**
     * 抽成计费周期
     */
    private String billTerm;

    /**
     * 费项
     */
    private Long feeItemTypeId ;

    /**
     * 费项名称
     */
    private String feeItemTypeName ;

    /**
     * 应收日期
     */
    private Integer day ;

    /**
     * 开始日期
     */
    private Long startDate;

    /**
     * 结束日期
     */
    private Long endDate;

    /**
     * 规则ID
     */
    private Long ruleId;

    /**
     * 规则名称
     */
    private String ruleName;

    /**
     * 参考对象
     */
    private String ruleObj;

    /**
     * 规则描述
     */
    private String ruleDesc;

}
