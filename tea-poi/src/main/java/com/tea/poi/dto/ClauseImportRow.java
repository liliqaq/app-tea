package com.tea.poi.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 条款导入Row对象
 * @author guanshaochi
 */
@Data
@ExcelIgnoreUnannotated
public class ClauseImportRow {

    /**
     * 合同编号
     */
    @ExcelProperty("合同编号")
    private String contractNo;

    /**
     * 本行行号
     */
    private int rowNum;
    //
    // /**
    //  * 资源Id
    //  */
    // private Integer resId;
    // /**
    //  * 资源全名
    //  */
    // private String resAllName;
    //
    // /**
    //  * 费项类别ID
    //  */
    // private Integer acctItemTypeId;
    // /**
    //  * 费项名称
    //  */
    // private String acctItemTypeName;
    // /**
    //  * 费项ID
    //  */
    // private Integer feeItemTypeId;
    // /**
    //  * 费项名称
    //  */
    // private String feeItemTypeName;
    // /**
    //  * 收费标准Id
    //  */
    // private Integer billRuleId;
    // /**
    //  * 收费标准名称
    //  */
    // private String ruleName;
    // /**
    //  * 计费开始日期
    //  */
    // private Long chargeStartDate;
    // /**
    //  * 计费结束日期
    //  */
    // private Long chargeEndDate;
    // /**
    //  * 单价
    //  */
    // private BigDecimal unitPrice;
    // /**
    //  * 金额
    //  */
    // private BigDecimal amount;
    // /**
    //  * 收费类型 (周期性费用区分按面积, 固定金额)
    //  */
    // private Integer billType;
    //
    // /**
    //  * 收费标准单位
    //  */
    // private String chargeUnit;
    //
    //
    // public String getDateRangeStr(){
    //     return this.getChargeStartDate() + "-" + this.getChargeEndDate();
    // }
}
