package com.tea.poi.poi;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.fastjson.JSON;
import com.tea.poi.dto.ClauseImportRow;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tea
 * @date 2022/11/18
 */
@Slf4j
public class ClauseListener implements ReadListener<ClauseImportRow> {

    @Override
    public void invoke(ClauseImportRow data, AnalysisContext context) {
        log.info(JSON.toJSONString(data));
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("读取完毕");
    }


}
