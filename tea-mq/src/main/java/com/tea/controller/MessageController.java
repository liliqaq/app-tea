package com.tea.controller;

import com.tea.service.OrderProduceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: tea
 * @date: 2020/12/4 5:30
 * @description:
 */
@RestController
public class MessageController {

    @Autowired
    private OrderProduceService service;

    @GetMapping("/send")
    void send(String m){
        service.send(m);
    }

}
