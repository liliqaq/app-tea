package com.tea.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: tea
 * @date: 2020/12/4 5:27
 * @description:
 */
@Configuration
public class RabbitQueueConfig {

    //队列过期时间
    private int orderQueueTTL = 5000;

    /**
     * 配置普通队列
     * @return
     */
    @Bean
    public Queue orderQueue() {
        return QueueBuilder.durable(RabbitConstants.ORDER_QUEUE)
                .ttl(orderQueueTTL)
                //设置死信队列的RouteKey
                .deadLetterRoutingKey(RabbitConstants.DEAD_ROUTE_KEY)
                //设置死信队列的Exchange
                .deadLetterExchange(RabbitConstants.DEAD_EXCHANGE)
                .build();
    }

    @Bean
    public TopicExchange orderTopicExchange() {
        return new TopicExchange(RabbitConstants.ORDER_EXCHANGE);
    }

    @Bean
    public Binding orderBinding() {
        return BindingBuilder.bind(orderQueue())
                .to(orderTopicExchange())
                .with(RabbitConstants.ORDER_ROUTE_KEY);
    }

    /**
     * 配置死信队列
     * @return
     */
    @Bean
    public Queue deadQueue() {
        return new Queue(RabbitConstants.DEAD_QUEUE, true);
    }

    @Bean
    public TopicExchange deadExchange() {
        return new TopicExchange(RabbitConstants.DEAD_EXCHANGE);
    }

    @Bean
    public Binding deadBinding() {
        return BindingBuilder.bind(deadQueue())
                .to(deadExchange())
                .with(RabbitConstants.DEAD_ROUTE_KEY);

    }
}
