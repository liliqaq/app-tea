package com.tea.config;

/**
 * @author: tea
 * @date: 2020/12/4 5:27
 * @description:
 */
public class RabbitConstants {
    /**
     * 订单队列
     */
    public static final String ORDER_ROUTE_KEY = "order_route_key";
    public static final String ORDER_EXCHANGE = "order_exchange";
    public static final String ORDER_QUEUE = "order_queue_test";

    /**
     * 死信队列
     */
    public static final String DEAD_QUEUE = "dead_queue";
    public static final String DEAD_EXCHANGE = "dead_exchange";
    public static final String DEAD_ROUTE_KEY = "dead_route_key";

}
