package com.tea.service;

import com.tea.config.RabbitConstants;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author: tea
 * @date: 2020/12/4 5:28
 * @description: 定义一个类，发送消息，接收消息，并进行消息确认回调
 */
@Service
public class OrderProduceService implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void setCallback() {
        rabbitTemplate.setReturnCallback(this);
        rabbitTemplate.setConfirmCallback(this);
        Runnable runnable = () -> send("这是我发送的测试消息，测试id=" + UUID.randomUUID().toString());
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(runnable, 0, 5, TimeUnit.SECONDS);
    }

    public void send(Object message) {
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        System.out.println(correlationData.getId());
        rabbitTemplate.convertAndSend(RabbitConstants.ORDER_EXCHANGE, RabbitConstants.ORDER_ROUTE_KEY, message, correlationData);
    }


    /**
     *
     * @param correlationData
     * @param ack 确认消息是否投递成功
     * @param s
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String s) {
        System.out.println("消息发送成功,发送ack确认,id=" + correlationData.getId());
        if (ack) {
            System.out.println("发送成功 ");
        } else {
            System.out.println("发送失败");
        }
    }

    /**
     * 失败回调
     * @param message 返回的消息。
     * @param replyCode 应答代码。
     * @param replyText 回复文本。
     * @param exchange 交换机
     * @param routingKey 路由键
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("消息丢失, 没有投递成功");
    }


    /**
     * 监听订单队列
     * @param message
     * @param channel
     * @throws IOException
     */
    @RabbitListener(queues = RabbitConstants.ORDER_QUEUE)
    public void orderQueueListener(Message message, Channel channel) throws IOException {
        String receivedRoutingKey = message.getMessageProperties().getReceivedRoutingKey();
        String msg = new String(message.getBody());
        System.out.println("路由key= [ " + receivedRoutingKey + " ]接收到的消息= [ " + msg + " ]");
        //Thread.sleep(5000);
        //发送ack给消息队列,收到消息了
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
    }

    /**
     * 监听私信队列
     * @param message
     * @param channel
     * @throws InterruptedException
     * @throws IOException
     */
    @RabbitListener(queues = RabbitConstants.DEAD_QUEUE)
    public void deadQueueListener(Message message, Channel channel) throws InterruptedException, IOException {
        String receivedRoutingKey = message.getMessageProperties().getReceivedRoutingKey();
        String msg = new String(message.getBody());
        System.out.println("路由key= [ " + receivedRoutingKey + " ]接收到的消息= [ " + msg + " ]");
        //Thread.sleep(5000);
        // 发送ack给消息队列，收到消息了
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);

    }
}
