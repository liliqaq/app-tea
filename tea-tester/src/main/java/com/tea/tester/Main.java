package com.tea.tester;

import cn.hutool.core.collection.ListUtil;
import com.alibaba.fastjson.JSON;
import com.tea.tester.dto.TestDto;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Random;

/**
 * 数据源
 *
 * @author tea
 * @date 2023/3/14
 */
@Slf4j
public final class Main {

    public static final Random RANDOM = new Random(1000);

    public static List<Integer> getIntegerList() {
        List<Integer> list = ListUtil.list(false);

        for (int i = 0; i < 20; i++) {
            list.add(RANDOM.nextInt());
        }

        return list;
    }

    public static List<Long> getLongList() {
        List<Long> list = ListUtil.list(false);

        for (int i = 0; i < 20; i++) {
            list.add(RANDOM.nextLong());
        }

        return list;
    }

    public static List<TestDto> getTestDtoList() {
        List<TestDto> list = ListUtil.list(false);

        for (int i = 0; i < 20; i++) {
            TestDto testDto = new TestDto();
            testDto.setChargeId(i);
            testDto.setChargeName(RANDOM.nextInt() + "");
            testDto.setApplyId(RANDOM.nextInt());
            list.add(testDto);
        }

        log.debug("obj集合：{}", JSON.toJSONString(list));
        return list;
    }

}
