package com.tea.tester.dto;

import lombok.Data;

/**
 * @author tea
 * @date 2023/3/16
 */
@Data
public class Dest {

    private String name;
    private int age;

    public Dest(String name, int age) {
        this.name = name;
        this.age = age;
    }

}
