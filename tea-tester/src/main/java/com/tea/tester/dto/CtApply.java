package com.tea.tester.dto;


import lombok.Data;

@Data
public class CtApply  {
    private Long applyId;

    private Long groupId;

    private Long organId;

    private Integer applyType;

    private String applyName;

    private Long newContractId;

    private String newContractNo;

    private Long oldContractId;

    private String oldContractNo;

    private Byte overType;

    private String overReason;

    private Long effectDate;

    private Long effectMoney;

    private Byte applyStatus;

    private String remarks;

    private Long apprId;

    private Byte status;

    private Long apprTime;

    private Long apprStartTime;

    private Long createUserId;

    private String createUserName;

    private Long createTime;

    private String secondName;

    private Integer suspendFlag;

    private Long lockBatchId;

    public void setApplyId(Long applyId) {
        this.applyId = applyId;
        int i = applyId.intValue();
        System.out.println("set");
    }
}