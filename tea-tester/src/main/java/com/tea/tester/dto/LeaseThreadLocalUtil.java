package com.tea.tester.dto;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * ThreadLocal 工具类
 *
 * @author tea
 * @date 2022/4/18
 */
@Slf4j
public class LeaseThreadLocalUtil {

    private static final ThreadLocal<Map<String, String>> threadLocal = new ThreadLocal<>();

    public static void clean() {
        log.debug("清除threadLocal");
        threadLocal.set(null);
        threadLocal.remove();
    }

    public static void setValue(String key, Object obj) {
        preProcess();
        log.debug("threadLocal设置值：{},{}", key, JSON.toJSONString(obj));

        if (obj instanceof String || obj instanceof Number || obj instanceof Boolean) {
            threadLocal.get().put(key, obj.toString());
        } else {
            threadLocal.get().put(key, JSON.toJSONString(obj));
        }
    }


    public static String getValue(String key) {
        log.debug("threadLocal获取值：{}", key);
        preProcess();
        String value = threadLocal.get().get(key);
        cleanValue(key);
        return value;
    }

    public static <T> T getValue(String key, Class<T> clazz) {
        log.debug("threadLocal获取值：{}：{}", key, clazz.getName());
        preProcess();
        String value = threadLocal.get().get(key);
        T v = JSON.parseObject(value, clazz);
        cleanValue(key);
        return v;
    }

    private static void preProcess() {
        log.debug("初始化threadLocal");
        if (MapUtil.isEmpty(threadLocal.get())) {
            log.debug("初始化threadLocal.map");
            threadLocal.set(new HashMap<>(16));
        }
    }

    public static void cleanValue(String key) {
        log.debug("threadLocal清空值：{}", key);
        threadLocal.get().remove(key);
    }
}
