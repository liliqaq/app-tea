package com.tea.tester.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tea
 * @date 2023/3/14
 */
@Data
public class TestDto implements Serializable {

    private static final long serialVersionUID = 8471690085132659777L;

    private Integer applyId;

    private Integer ctChargeId;

    private Integer chargeId;

    private String chargeName;


    private Byte isMonthSplit;

    private String feeItemTypeName;

    private Integer billingUnit;


}
