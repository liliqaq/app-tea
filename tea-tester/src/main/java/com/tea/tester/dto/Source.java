package com.tea.tester.dto;

import lombok.Data;

/**
 * @author tea
 * @date 2023/3/16
 */
@Data
public class Source {

    private String name;
    private int age;

    public Source(String name, int age) {
        this.name = name;
        this.age = age;
    }


}
