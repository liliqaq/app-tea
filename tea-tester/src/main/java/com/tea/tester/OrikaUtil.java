package com.tea.tester;

import ma.glasnost.orika.Converter;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;

import java.util.List;
import java.util.Map;

/**
 * Orika转换工具类
 *
 * @author tea
 * @date 2021/6/7
 */
public class OrikaUtil {

    private static final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

    public static <A, B> ClassMapBuilder<A, B> classMap(Class<A> source, Class<B> target) {
        return mapperFactory.classMap(source, target);
    }

    public static <T> T convert(Object source, Class<T> target) {
        return mapperFactory.getMapperFacade().map(source, target);
    }

    public static <S, D> List<D> convertList(Iterable<S> source, Class<D> target) {
        return mapperFactory.getMapperFacade().mapAsList(source, target);
    }

    public static MapperFactory getSingletonMapperFactory() {
        return mapperFactory;
    }

    public static MapperFactory getPrototypeFactory() {
        return new DefaultMapperFactory.Builder().mapNulls(false).build();
    }

    /**
     * 注册转换器
     *
     * @param mapperFactory Orika主要配置
     * @param converterMap  转换器集合
     */
    public static void registerConverter(MapperFactory mapperFactory, Map<String, Converter> converterMap) {
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();

        converterMap.keySet().forEach(converter -> converterFactory.registerConverter(converter, converterMap.get(converter)));
    }
}
