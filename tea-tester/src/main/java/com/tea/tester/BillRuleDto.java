package com.tea.tester;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrator
 * @date 2018年7月3日19:28:10
 * 收费标准Dto
 */
@Data
public class BillRuleDto implements Serializable {
    private static final long serialVersionUID = -2891997155112145281L;

    private Long contractId;
    private String billRuleId;
    private String billingUnit;
    private String effDate;
    private String expDate;
    private String feeItemTypeId;
    private String feePrecision;
    private String isNormalMonth;
    private String rateModelType;
    private String remark;
    private String ruleName;
    private String ruleTypeId;
    private String state;
    /**
     * 租金应收日
     */
    private Integer limitDays;
    private List<BillRuleTafficDto> ruleTafficList;

    private Integer billType;

    private Integer billRuleType;
}
