package com.tea.tester;

import com.tea.tester.dto.TestDto;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author tea
 * @date 2023/4/4
 */
public class Tester {

    public static void main(String[] args) throws Exception {
        List<TestDto> testDtoList = Main.getTestDtoList();

        Map<Integer, TestDto> collect = testDtoList.stream().collect(Collectors.toMap(TestDto::getChargeId, Function.identity()));
        collect.get(1).setChargeId(5);
        System.out.println(testDtoList.get(0));
    }


}
