package com.tea.tester;

import java.io.Serializable;

/**
 * @author Administrator
 * @date 2018年7月3日19:26:13
 * 费率Dto
 */
public class BillRuleTafficDto implements Serializable {
    private static final long serialVersionUID = 8335693791657912467L;
    /*费率ID*/
    private String formulaId;
     /*费率*/
     private String rate;
     /*费率描述*/
     private String rateDesc;

    public String getFormulaId() {
        return formulaId;
    }

    public void setFormulaId(String formulaId) {
        this.formulaId = formulaId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRateDesc() {
        return rateDesc;
    }

    public void setRateDesc(String rateDesc) {
        this.rateDesc = rateDesc;
    }
}
