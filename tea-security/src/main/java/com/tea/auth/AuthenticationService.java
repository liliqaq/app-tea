package com.tea.auth;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tea.entity.RoleEntity;
import com.tea.entity.UserEntity;
import com.tea.mapper.RoleMapper;
import com.tea.mapper.UserMapper;
import com.tea.util.PasswordEncoderUtil;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: tea
 * @date: 2020/12/15 21:05
 * @description:
 */
@Service
@Primary
public class AuthenticationService implements UserDetailsService {

    private final UserMapper userMapper;
    private final RoleMapper roleMapper;

    public AuthenticationService(UserMapper userMapper, RoleMapper roleMapper) {
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // 查询用户
        UserEntity userEntity = userMapper.selectOne(new QueryWrapper<UserEntity>().eq("username", username));
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        Long roleId = userEntity.getRoleId();

        RoleEntity roleEntity = roleMapper.selectById(roleId);
        String roleName = roleEntity.getRoleName();
        // 添加权限
        authorities.add(new SimpleGrantedAuthority(roleName));

        return new User(userEntity.getUsername(), userEntity.getPassword(), authorities);
    }

    public void registry(UserEntity userEntity) {
        if (userEntity.getRoleId() == null) {
            userEntity.setRoleId(1L);
        }
        userEntity.setPassword(PasswordEncoderUtil.passwordEncoder(userEntity.getPassword()));
        userMapper.insert(userEntity);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public int add() {
        try {
            RoleEntity roleEntity = new RoleEntity();
            roleEntity.setRoleName("admin");
            roleMapper.insert(roleEntity);
            int i = 4 / 0;
            return i;
        } catch (Exception e) {
            throw e;
        } finally {
            RoleEntity roleEntity = new RoleEntity();
            roleEntity.setRoleName("finally");
            roleMapper.insert(roleEntity);
        }
    }
}
