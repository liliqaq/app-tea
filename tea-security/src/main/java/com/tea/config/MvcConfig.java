package com.tea.config;

import com.tea.config.properties.ApplicationConfigure;
import com.tea.interceptor.LoggerInterceptor;
import com.tea.interceptor.PathInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author：tea
 * @date: 2020/9/15 22:17
 * @description: MVC配置类
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

	@Autowired
	private ApplicationConfigure appconfig;

	/**
	 * 自定义静态资源映射
	 *
	 * @param registry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/swagger-ui.html")
				.addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/doc.html")
				.addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry.addResourceHandler("/static/**")
				.addResourceLocations("classpath:/static/");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/sign_in").setViewName("login");
		registry.addViewController("/sign_up").setViewName("regist");
	}

	/**
	 * 视图解析器
	 *
	 * @param registry
	 */
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.viewResolver(resourceViewResolver());
	}

	@Bean
	public InternalResourceViewResolver resourceViewResolver() {
		InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
		// 请求视图文件的前缀地址
		// internalResourceViewResolver.setPrefix("/template/");
		// // 请求视图文件的后缀
		// internalResourceViewResolver.setSuffix(".html");
		return internalResourceViewResolver;
	}

	/**
	 * 添加拦截器
	 *
	 * @param registry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 过滤静态资源
		List<String> excludeStatic = new ArrayList<String>();
		excludeStatic.add("/css/**");
		excludeStatic.add("/framework/**");
		excludeStatic.add("/images/**");
		excludeStatic.add("/js/**");
		excludeStatic.add("/json/**");
		excludeStatic.add("/plugins/**");

		registry.addInterceptor(new LoggerInterceptor()).addPathPatterns("/**").excludePathPatterns(excludeStatic);
		registry.addInterceptor(new PathInterceptor(appconfig)).addPathPatterns("/**").excludePathPatterns(excludeStatic);
	}

	/**
	 * 添加跨域请求
	 *
	 * @param registry
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				// 配置允许访问的跨域资源的请求域名
				.allowedOrigins("*")
				// 配置允许访问该跨域资源服务器的请求方法
				.allowedHeaders("*")
				// 配置允许请求头部head的访问
				.allowedHeaders("*");
	}

	/**
	 * 将接受的参数解析成指定的格式
	 *
	 * @param registry
	 */
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new DateFormatter("yyyy-MM-dd"));
	}
}
