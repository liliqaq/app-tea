package com.tea.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author tea
 */
@Data
@Component
@ConfigurationProperties(prefix = "tea.application")
public class ApplicationConfigure {

    /**
     * - 项目名称
     */
    private String name;

    /**
     * - 项目简称
     */
    private String abbreviation;

    /**
     * - 项目版本
     */
    private String version;

    /**
     * - 密集开关
     */
    private boolean denseSwitch;

    /**
     * - 密集标识
     */
    private String denseIdentification;
}
