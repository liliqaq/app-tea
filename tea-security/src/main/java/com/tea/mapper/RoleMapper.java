package com.tea.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tea.entity.RoleEntity;
import org.springframework.stereotype.Repository;

/**
 * @author: tea
 * @date: 2020/12/15 21:10
 * @description:
 */
@Repository
public interface RoleMapper extends BaseMapper<RoleEntity> {

}
