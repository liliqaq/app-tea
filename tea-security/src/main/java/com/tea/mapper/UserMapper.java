package com.tea.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tea.entity.UserEntity;
import org.springframework.stereotype.Repository;

/**
 * @author: tea
 * @date: 2020/12/15 21:07
 * @description:
 */
@Repository
public interface UserMapper extends BaseMapper<UserEntity> {


}
