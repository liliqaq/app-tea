package com.tea.controller;

import com.tea.auth.AuthenticationService;
import com.tea.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tea
 * @date 2020/12/15 21:26
 */
@Controller
public class LoginController {

    @Autowired
    private AuthenticationService authenticationService;


    @ResponseBody
    @RequestMapping("/loginSuccess")
    public void login(HttpServletRequest request) {
        System.out.println("登录成功");
    }

    @ResponseBody
    @PostMapping("/registry")
    public void registry(UserEntity userEntity) {
        UserEntity userEntity1 = new UserEntity();
        userEntity1.setUsername(userEntity.getUsername());
        userEntity1.setPassword(userEntity.getPassword());
        authenticationService.registry(userEntity1);
    }

    @GetMapping("/sign_fail")
    @ResponseBody
    String signFail() {
        return "fail";
    }

    @GetMapping("/add")
    @ResponseBody
    public void add() {
        authenticationService.add();
    }
}
