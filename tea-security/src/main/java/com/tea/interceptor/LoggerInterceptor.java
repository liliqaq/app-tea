package com.tea.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;


/**
 * @author tea
 */
@Slf4j
public class LoggerInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		long startTime = System.currentTimeMillis();
		request.setAttribute("startTime", startTime);
		String taskId = generate();
		request.setAttribute("taskId", taskId);
		
		if(handler instanceof HandlerMethod) {
			StringBuilder sb = new StringBuilder(1000);
			sb.append("\n==========================================")
			  .append(getTime())
			  .append("==========================================\n");
			HandlerMethod method = (HandlerMethod) handler;
			sb.append("Controller: ").append(method.getBean().getClass().getName()).append("\n");
			sb.append("Method:     ").append(method.getMethod().getName()).append("\n");
			sb.append("Parmas:     ").append(getParamString(request.getParameterMap())).append("\n");
			sb.append("URI:        ").append(request.getRequestURI()).append("\n");
			sb.append("TASKID:     ").append(taskId).append("\n");
			log.debug(sb.toString());
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
		long startTime = (Long) request.getAttribute("startTime");
		String taskId = (String) request.getAttribute("taskId");
		long endTime = System.currentTimeMillis();
		
		if(handler instanceof HandlerMethod) {
			StringBuilder sb = new StringBuilder(1000);
			long excTime = endTime - startTime;
			
			if(excTime > 1000) {
				String warning = "★★★";
				sb.append("[" + taskId + "] - " + (excTime > 1000 ? warning : "") + "Using:  ").append(excTime).append("ms").append("\n");
				sb.append("------------------------------------------------------------------------------------");
				log.warn(sb.toString());
			} else {
				sb.append("[" + taskId + "] - " +  "Using:  ").append(excTime).append("ms").append("\n");
				sb.append("------------------------------------------------------------------------------------");
				log.debug(sb.toString());
			}
		}
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
	private String getParamString(Map<String, String[]> map) {
		StringBuilder sb = new StringBuilder();
		for(Entry<String, String[]> e : map.entrySet()) {
			sb.append(e.getKey()).append("=");
			String[] values = e.getValue();
			if(values != null && values.length == 1) {
				sb.append(values[0]).append(" \t");
			} else {
				sb.append(Arrays.toString(values)).append(" \t");
			}
		}
		return sb.toString();
	}
	
	private String getTime() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(new Date());
	}
	
	private static String generate() {
		
		UUID uuid = UUID.randomUUID();
		String id = uuid.toString();
		return id.replace("-", "");
	}

}
