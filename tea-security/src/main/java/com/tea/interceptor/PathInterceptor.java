package com.tea.interceptor;

import com.tea.config.properties.ApplicationConfigure;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author tea
 */
@Component
public class PathInterceptor implements HandlerInterceptor {

	private ApplicationConfigure appConfig;

	public PathInterceptor(ApplicationConfigure appConfig) {
		this.appConfig = appConfig;
	}

	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		String path = request.getContextPath();
        String scheme = request.getScheme();
        String serverName = request.getServerName();
        int port = request.getServerPort();
        String basePath = scheme + "://" + serverName + ":" + port + path;

//        request.setAttribute("basePath", basePath);
        request.setAttribute("basePath", path);
        request.setAttribute("path", path);
        request.setAttribute("appInfo", appConfig);
        return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
}

