package com.tea.service.impl;

import com.tea.service.HelloService;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author: tea
 * @date: 2020/12/16 18:36
 * @description:
 */
@Service
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String name) {
        return "hello: \t" + name;
    }
}
