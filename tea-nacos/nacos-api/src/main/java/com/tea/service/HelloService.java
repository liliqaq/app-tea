package com.tea.service;

/**
 * @author: tea
 * @date: 2020/12/16 18:34
 * @description:
 */
public interface HelloService {

    /**
     * sayHello
     * @param name
     * @return
     */
    String sayHello(String name);
}
