package com.tea.controller;

import com.tea.service.HelloService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: tea
 * @date: 2020/12/16 18:43
 * @description:
 */
@RestController
public class HelloController {

    @Reference
    private HelloService helloService;

    @GetMapping("/sayHello")
    String sayHello(){
        return helloService.sayHello("tea");
    }

}
