package com.tea.model;

import lombok.Data;

/**
 * @author tea
 * @date 2022/2/24
 */
@Data
public class Address {

	private Integer id;

	private String address;

}
