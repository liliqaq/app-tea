package com.tea.model;

import lombok.Data;

/**
 * @author tea
 * @date 2022/2/24
 */
@Data
public class User {

	private Integer id;

	private String name;

	private Address address;

}
