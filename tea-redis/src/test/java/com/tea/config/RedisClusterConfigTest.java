package com.tea.config;

import com.tea.RedisClusterApplication;
import com.tea.model.Address;
import com.tea.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.JedisCluster;

/**
 * @author tea
 * @date 2022/2/22
 */
@SpringBootTest(classes = RedisClusterApplication.class)
class RedisClusterConfigTest {

	@Autowired
	private RedisTemplate<String, Object> template;

	@Autowired
	private JedisCluster jedisCluster;

	@Test
	void test1() {
		String set = jedisCluster.set("s", "a");
		System.out.println(set);
		System.out.println(jedisCluster.get("s"));

	}



	@Test
	void test2() {
		User user = new User();
		user.setId(1);
		user.setName("root");
		Address address = new Address();
		address.setId(10001);
		address.setAddress("江苏");
		user.setAddress(address);

		template.opsForValue().set("user", user);
		Object s = template.opsForValue().get("user");
		System.out.println(s.toString());

	}

}