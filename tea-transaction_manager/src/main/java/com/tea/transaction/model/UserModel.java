package com.tea.transaction.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author tea
 * @date 2021/8/31
 */
@Data
@TableName("user")
public class UserModel {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private Integer age;

}
