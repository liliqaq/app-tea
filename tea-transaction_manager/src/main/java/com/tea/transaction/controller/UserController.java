package com.tea.transaction.controller;

import com.tea.transaction.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author tea
 * @date 2021/8/31
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/insert/{i}")
    @ResponseBody
    public String insert(@PathVariable("i") Integer i) {
        userService.insertBatch(i);
        return "1";
    }

    @RequestMapping("/s")
    public String s() {
        return "redirect:https://www.baidu.com";
    }

}
