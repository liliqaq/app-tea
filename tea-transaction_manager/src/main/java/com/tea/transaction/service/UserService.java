package com.tea.transaction.service;

import cn.hutool.core.collection.ListUtil;
import com.tea.transaction.dao.UserDao;
import com.tea.transaction.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tea
 * @date 2021/8/31
 */
@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private ApplicationContext applicationContext;

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void insert(UserModel userModel) {
        userDao.insert(userModel);
        if (userModel.getAge() > 5) {
            throw new RuntimeException();
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void insertBatch(Integer j) {
        UserModel userModel = new UserModel();
        List<Integer> list = ListUtil.list(false);
        for (int i = 0; i < j; i++) {
            list.add(i);
        }

        list.forEach(e -> {
            userModel.setId(e);
            userModel.setName(e + "");
            userModel.setAge(e);
            UserService service = applicationContext.getBean(UserService.class);
            try {
                service.insert(userModel);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });

        logger.info("子事务已完成");

        userDao.deleteById(0);
    }

}
