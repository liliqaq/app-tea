package com.tea.transaction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tea.transaction.model.UserModel;
import org.springframework.stereotype.Repository;

/**
 * @author tea
 * @date 2021/8/31
 */
@Repository
public interface UserDao extends BaseMapper<UserModel> {

}
