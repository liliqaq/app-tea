package com.tea.controller;

import com.tea.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: tea
 * @date: 2020/12/16 20:40
 * @description:
 */
@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;

    @GetMapping("/sayHello")
    String sayHello() {
        return helloService.sayHello("tea");
    }

}
