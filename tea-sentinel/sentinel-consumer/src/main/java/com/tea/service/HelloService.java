package com.tea.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author: tea
 * @date: 2020/12/16 20:40
 * FeignClient：调用的服务名称
 */
@Component
@FeignClient("sentinel-provider")
public interface HelloService {

    /**
     * sayHello
     * @param name
     * @return
     */
    @GetMapping("/sayHello")
    String sayHello(@RequestParam("name") String name);

}
