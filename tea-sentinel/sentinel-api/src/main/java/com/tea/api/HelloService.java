package com.tea.api;

/**
 * @author: tea
 * @date: 2020/12/15 18:46
 * @description:
 */
public interface HelloService {

    /**
     * sayHello
     * @param name
     * @return
     */
    String sayHello(String name);

}
