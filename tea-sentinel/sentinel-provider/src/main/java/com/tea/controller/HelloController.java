package com.tea.controller;

import com.tea.api.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: tea
 * @date: 2020/12/15 19:15
 * @description:
 */
@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;

    @GetMapping("/sayHello")
    String sayHello(String name) {
        return helloService.sayHello(name);
    }

}
