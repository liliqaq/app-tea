package com.tea.service;

import com.tea.api.HelloService;
import org.springframework.stereotype.Service;

/**
 * @author: tea
 * @date: 2020/12/15 19:13
 * @description:
 */
@Service
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String name) {
        return "hello: \t" + name;
    }
}
