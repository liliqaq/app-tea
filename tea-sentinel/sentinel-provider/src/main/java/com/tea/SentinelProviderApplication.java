package com.tea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: tea
 * @date: 2020/12/15 19:06
 * @description:
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentinelProviderApplication.class, args);
    }


}
