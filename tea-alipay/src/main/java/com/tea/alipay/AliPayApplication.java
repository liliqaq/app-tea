package com.tea.alipay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tea
 * @date 2023/5/7
 */
@SpringBootApplication
public class AliPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliPayApplication.class, args);
    }

}
