package com.tea.alipay.controller;

import cn.hutool.core.lang.UUID;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.tea.alipay.constans.AliPayConfig;
import com.tea.alipay.domain.AliPayFaceToFaceModel;
import com.tea.alipay.util.AliPayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tea
 * @date 2023/5/7
 */
@RestController
@Slf4j
public class AliPayFaceToFaceController {

    /**
     * 预付款下单
     */
    @GetMapping("face")
    public Map<String, Object> ZFBPreorderAction() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // (必填)商户唯一订单编号
            String outTradeNo = UUID.fastUUID().toString();
            // (必填) 订单标题，粗略描述用户的支付目的。如“喜士多（浦东店）消费”
            String subject = "tea 消费（中国）";
            // (必填) 订单总金额，单位为元，不能超过1亿元
            String totalAmount = "0.01";
            //（必填）支付成功支付支付宝异步通知的接口地址
            String notifyUrl = AliPayConfig.NOTIFY_URL;
            // 将参数放入实体对象中
            AliPayFaceToFaceModel aliPayFaceToFaceModel = new AliPayFaceToFaceModel();
            aliPayFaceToFaceModel.setOutTradeNo(outTradeNo);
            aliPayFaceToFaceModel.setSubject(subject);
            aliPayFaceToFaceModel.setTotalAmount(totalAmount);
            aliPayFaceToFaceModel.setNotifyUrl(notifyUrl);

            // 支付宝预下单
            String json = this.faceToFace(aliPayFaceToFaceModel);
            // 解析json数据
            JSONObject jsonObject = JSONObject.parseObject(json);
            // 得到alipay_trade_precreate_response数据后再强转JSONObject
            JSONObject jsonobj_two = (JSONObject) jsonObject.get("alipay_trade_precreate_response");
            // 再通过jsonobj_two获取到二维码地址
            String qrcode = jsonobj_two.get("qr_code").toString();

            resultMap.put("qrcode", qrcode);
            resultMap.put("outTradeNo", outTradeNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultMap;
    }

    /**
     * 支付宝预下单
     */
    private String faceToFace(@RequestBody AliPayFaceToFaceModel aliPayFaceToFaceModel) {
        try {
            AlipayClient aliPayClient = AliPayUtil.getAliPayClient();

            /** 实例化具体API对应的request类，类名称和接口名称对应,当前调用接口名称：alipay.trade.precreate（统一收单线下交易预创建（扫码支付）） **/
            AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();

            /** 设置业务参数  **/
            AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();

            /** 商户订单号，商户自定义，需保证在商户端不重复，如：20200612000001 **/
            model.setOutTradeNo(aliPayFaceToFaceModel.getOutTradeNo());

            /**订单标题 **/
            model.setSubject(aliPayFaceToFaceModel.getSubject());

            /** 订单金额，精确到小数点后两位 **/
            model.setTotalAmount(aliPayFaceToFaceModel.getTotalAmount());

            /** 订单描述 **/
            model.setBody(aliPayFaceToFaceModel.getBody());

            /** 业务扩展参数 **/
            //ExtendParams extendParams = new ExtendParams();

            /** 系统商编号，填写服务商的PID用于获取返佣，返佣参数传值前提：传值账号需要签约返佣协议，用于isv商户。 **/
            //extendParams.setSysServiceProviderId("2088511****07846");

            /** 花呗分期参数传值前提：必须有该接口花呗收款准入条件，且需签约花呗分期 **/
            /** 指定可选期数，只支持3/6/12期，还款期数越长手续费越高 **/
            // extendParams.setHbFqNum("3");

            /** 指定花呗分期手续费承担方式，手续费可以由用户全承担（该值为0），也可以商户全承担（该值为100），但不可以共同承担，即不可取0和100外的其他值。 **/
            //extendParams.setHbFqSellerPercent("0");

            //model.setExtendParams(extendParams);

            /** 将业务参数传至request中 **/
            request.setBizModel(model);

            /** 异步通知地址，以http或者https开头的，商户外网可以post访问的异步地址，用于接收支付宝返回的支付结果，如果未收到该通知可参考该文档进行确认：https://opensupport.alipay.com/support/helpcenter/193/201602475759 **/
            request.setNotifyUrl(aliPayFaceToFaceModel.getNotifyUrl());

            /**第三方调用（服务商模式），传值app_auth_token后，会收款至授权app_auth_token对应商家账号，如何获传值app_auth_token请参考文档：https://opensupport.alipay.com/support/helpcenter/79/201602494631 **/
            //request.putOtherTextParam("app_auth_token", "传入获取到的app_auth_token值");

            /** 通过alipayClient调用API，获得对应的response类  **/
            AlipayTradePrecreateResponse response = aliPayClient.execute(request);

            /** 获取接口调用结果，如果调用失败，可根据返回错误信息到该文档寻找排查方案：https://opensupport.alipay.com/support/helpcenter/101 **/
            System.out.println(response.getBody());
            return response.getBody();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String findZFB_trade(AliPayFaceToFaceModel aliPayFaceToFaceModel) throws Exception {

        AlipayClient aliPayClient = AliPayUtil.getAliPayClient();

        // 实例化具体API对应的request类，类名称和接口名称对应,当前调用接口名称：alipay.trade.query（统一收单线下交易查询）
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();

        // 设置业务参数
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();

        // 注：交易号（TradeNo）与订单号（OutTradeNo）二选一传入即可，如果2个同时传入，则以交易号为准
        // 支付接口传入的商户订单号。如：2020061601290011200000140004
        model.setOutTradeNo(aliPayFaceToFaceModel.getOutTradeNo());

        // 异步通知/查询接口返回的支付宝交易号，如：2020061622001473951448314322
        //model.setTradeNo("2020061622001473951448314322");

        // 将业务参数传至request中
        request.setBizModel(model);

        // 第三方调用（服务商模式），必须传值与支付接口相同的app_auth_token
        //request.putOtherTextParam("app_auth_token", "传入获取到的app_auth_token值");

        // 通过alipayClient调用API，获得对应的response类
        AlipayTradeQueryResponse response = aliPayClient.execute(request);

        // 获取接口调用结果，如果调用失败，可根据返回错误信息到该文档寻找排查方案：https://opensupport.alipay.com/support/helpcenter/101
        return response.getBody();
    }

    @RequestMapping("/findZFB_tradeAction")
    @ResponseBody
    public Map<String, Object> findZFB_tradeAction(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            //(必填)商户唯一订单编号
            String outTradeNo = request.getParameter("outTradeNo");
            AliPayFaceToFaceModel aliPayFaceToFaceModel = new AliPayFaceToFaceModel();
            aliPayFaceToFaceModel.setOutTradeNo(outTradeNo);
            //查询交易状态
            String json = this.findZFB_trade(aliPayFaceToFaceModel);
            System.out.println(json);
            JSONObject jsonObject = JSONObject.parseObject(json);
            JSONObject jsonobj_two = (JSONObject) jsonObject.get("alipay_trade_query_response");
            //网关返回码,详见文档 https://opendocs.alipay.com/open/common/105806
            String ZFBCode = (String) jsonobj_two.get("code");
            //业务返回码
            String ZFBSubCode = (String) jsonobj_two.get("sub_code");
            //业务返回码描述
            String sub_msg = (String) jsonobj_two.get("sub_msg");
            //交易状态：WAIT_BUYER_PAY（交易创建，等待买家付款）、TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）、TRADE_SUCCESS（交易支付成功）、TRADE_FINISHED（交易结束，不可退款）
            String trade_status = (String) jsonobj_two.get("trade_status");
            if (ZFBCode.equals("40004") && ZFBSubCode.equals("ACQ.TRADE_NOT_EXIST")) {
                //订单未创建（用户未扫码）
                resultMap.put("code", ZFBCode);
                resultMap.put("data", "用户未扫码");
            } else if (ZFBCode.equals("10000") && trade_status.equals("WAIT_BUYER_PAY")) {
                //订单已经创建但未支付（用户扫码后但是未支付）
                resultMap.put("code", ZFBCode);
                resultMap.put("data", "non-payment");
            } else if (ZFBCode.equals("10000") && (trade_status.equals("TRADE_SUCCESS") || trade_status.equals("TRADE_FINISHED"))) {
                //判断ZFBCode是否等于”10000“ 并且 trade_status等于TRADE_SUCCESS（交易支付成功）或者 trade_status等于TRADE_FINISHED（交易结束，不可退款）
                //订单已支付（用户扫码完成并且支付成功之后）
                resultMap.put("code", ZFBCode);
                resultMap.put("data", "yes-payment");
            } else {
                resultMap.put("code", ZFBCode);
                resultMap.put("data", sub_msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultMap;
    }

}
