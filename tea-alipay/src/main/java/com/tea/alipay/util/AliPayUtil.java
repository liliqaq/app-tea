package com.tea.alipay.util;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.tea.alipay.constans.AliPayConfig;

/**
 * @author tea
 * @date 2023/5/7
 */
public class AliPayUtil {

    public static AlipayClient getAliPayClient() {

        // 支付宝网关
        String URL = AliPayConfig.OPEN_API_DOMAIN;

        // 应用id，如何获取请参考：https://opensupport.alipay.com/support/helpcenter/190/201602493024
        String APP_ID = AliPayConfig.APP_ID;

        // 应用私钥，如何获取请参考：https://opensupport.alipay.com/support/helpcenter/207/201602469554
        String APP_PRIVATE_KEY = AliPayConfig.MERCHANT_PRIVATE_KEY;

        // 支付宝公钥，如何获取请参考：https://opensupport.alipay.com/support/helpcenter/207/201602487431
        String ALIPAY_PUBLIC_KEY = AliPayConfig.ALIPAY_PUBLIC_KEY;

        // 初始化
        return new DefaultAlipayClient(URL, APP_ID, APP_PRIVATE_KEY, "json", AliPayConfig.CHARSET, ALIPAY_PUBLIC_KEY, AliPayConfig.SIGN_TYPE);
    }

}
