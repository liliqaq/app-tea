package com.tea.alipay.constans;

/**
 * @author tea
 * @date 2023/5/7
 */
public class AliPayConfig {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static final String APP_ID = "2021000122688122";

    public static final String PID = "2088721012919307";

    // 调用网关
    public static final String OPEN_API_DOMAIN = "https://openapi.alipaydev.com/gateway.do";


    // 商户私钥，您的PKCS8格式RSA2私钥
    public static final String MERCHANT_PRIVATE_KEY = "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDBUjj08MpiIxEFDxUlsN3ipcRk2JFObNVcJD6wiPx2Rx6Ivd7uTps+ViQA/vE3oMLcrnIvm5asN/hBfhWdsIsDigiOmxT2JNeYv9PJvHJ8WyMrkstLJkICCkBd7lCxXwnxWXRJUzC/GawfJo+6QeL6B6sNdiSFM+LAEWDfedUAs3z6MkPhxzTXeq0/PnejSAlTaoNNY14dZBRt4Lx/bNZ4rC1/CPt3+p1SZ/WFOQUlCSGIY9815gXOheAd/j1mNii055Q3SPIFSycZmHtNfjebTvYm6XGrBPh7fIZ+40Ubv+SfyZ+DXoRsxUitBFWAC4fcs6/9o2vPYRgbhKhl0aVzAgMBAAECggEBALssfcMCBCFibE8mnEYC6cJQ10cAvCdfjnCJEz1fVHi8QR8KHvdCmmKPioqATFKczy5VpG7dyNNz2WSnzAwCa4YAtmpEUtZDM49MY6UWVQ58OyAsH7LiFtTc0hdf7zbcmsbX4pQ//Jl6JMGAV3Zx5m5I0F/XPGLPqsJl0RJuZkIm1pElEa8PY/823fHL7Le5pC0r8dqeMCSEcqMOck2ipr4BJSEvdezjkqMt9WC8W++BQ74USsljpsxqtGn59Ofbn5dTwVhagt4tm9a2FGiZsNGqekE+V/fR5Uh1uTAwZuvlaQUZSKTmdtH1ro0pVVfobT9+KyNAU1zJBC0FJ9plN9ECgYEA7l4hZz2lxNowLW3oF7MLDSj4Z8I3Jl60YBRAIRAC0psR6A/yUdD8d2/o7/smaydhBQc1c0q/ABE726Hjh9CQvkgFUF2S+XKUgnSSvNzqplZSYqLa4mgdja2RH4U8TddnvcFGIVdKzNYwNaV+xDlzvEo9DiiX3/39sav72C5QZrsCgYEAz58QoOoFB8rdVGAy7gSmuJvxe/IA4VwW24XccO11qDc3jQpmz7Ag/NbD+gzlVgGfLIlITaUk8qwTdLN/DB/8hb0tPTiptp1Wnk0WUjQ2TPbZgDIiYVL2NhOHzOUHkz0lPr06Wk8VcvW/7ClsiRfFCYm+QwhD2EqEnsbunKZePKkCgYEAzKXQM9z/lzRkLHyxyLcVA+Vq71oVE0I3xk1ciYY/hiVQiFY4ivnL55cS/AY6A8E5UKHRx/wiY6JoK7afrBjqTBN1THwqFTPEQQRofvGEG+78qGZ9Wehdhye+8rrm/UW0cXugBDv3okk59rTxUvKoZL2wEZnUWqCr0ICBpiaNLMUCgYEAp3eINCNPF2/8gthtfCtoRzQR3oXfS+e50CvzGWuQIbUYK72eHBvXH9ojjYflXDBGseex2F9CpDNEJje6XI9YYo+q89zGl7PlcB6ByKCww/fAjHTj4ljGm+u49/4s1AjYB/keHkX36RhsTBNFpnbzybO/MdZy0v2YZ3HSeayACrECgYEAsaRHxTWskUtlHIYglxjj4tTgVPVdWrqIeN68cI3lhNrSSN5uGtXhqWRXSdrbZV5hixpS+p8LmSswUO7K5nmLPKBqQ594DNyQrEimjlOk9XLNdMVMOCUYm6o3o39GSL9l5tqkoqXQGMh6E9Lfxh/yHkf8z9UhXqj0FHgVjj1t+ug=";

    // 商户公钥
    public static final String MERCHANT_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwVI49PDKYiMRBQ8VJbDd4qXEZNiRTmzVXCQ+sIj8dkceiL3e7k6bPlYkAP7xN6DC3K5yL5uWrDf4QX4VnbCLA4oIjpsU9iTXmL/TybxyfFsjK5LLSyZCAgpAXe5QsV8J8Vl0SVMwvxmsHyaPukHi+gerDXYkhTPiwBFg33nVALN8+jJD4cc013qtPz53o0gJU2qDTWNeHWQUbeC8f2zWeKwtfwj7d/qdUmf1hTkFJQkhiGPfNeYFzoXgHf49ZjYotOeUN0jyBUsnGZh7TX43m072JulxqwT4e3yGfuNFG7/kn8mfg16EbMVIrQRVgAuH3LOv/aNrz2EYG4SoZdGlcwIDAQAB";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAllEOhfCSJd2GP2BEescT/WGfZ9ybdLWg3XDtra7ONUmR8COHHx7cvcVwuqebC5CVFhcyEYmqNG370Q1HX6UKEJvB1/0+Vo+Jpwh8MLukorWv5S7378a97b4BAk+eefSrKaYSHoFSq+Kc11HI1f6DwcZDxQeizKbd3+mVgDh33weHC+ovIVjf2YBzVTfcTwi3eSle4XCndAsMoTukfFa7+A0P3d0lZSqOfjYgE8FowZO2d9UFS+ICzBSlslfZiGEdmGi0f6paq9gKgoQIJ7I6ctxLuaHtwjnRQwrvOIJvVtM2HsmzikavhZPacxHobncCq+CAJpVp9sXZwFC5hOvVZQIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // public static final String NOTIFY_URL = "http://127.0.0.1/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static final String RETURN_URL = "http://127.0.0.1/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

    // 签名方式
    public static final String SIGN_TYPE = "RSA2";

    // 当面付最大查询次数和查询间隔（毫秒）
    public static final int MAX_QUERY_RETRY = 5;
    public static final int QUERY_DURATION = 5000;

    // 当面付最大撤销次数和撤销间隔（毫秒）
    public static final int MAX_CANCEL_RETRY = 3;
    public static final int CANCEL_DURATION = 2000;

    // 交易保障线程第一次调度延迟和调度间隔（秒）
    public static final int HEARTBEAT_DELAY = 5;
    public static final int HEARTBEAT_DURATION = 900;

    // 异步通知url(注意拦截器是否拦截)
    public static final String NOTIFY_URL = "http://8u862c.natappfree.cc/NBOSTA_WSBM/Alipay/ZFBcallbackAction.do";

    // 字符编码格式
    public static final String CHARSET = "utf-8";


}
