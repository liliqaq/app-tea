package com.tea.client;

/**
 * @author tea
 * @date 2021/2/25
 */
public class Demo {

    public static void main(String[] args) throws Exception {
        HelloServiceImplService helloService = new HelloServiceImplServiceLocator();
        String hello = helloService.getHelloServicePort().hello("web service !");
        System.out.println("*****************************************");
        System.out.println(hello);
    }

}
