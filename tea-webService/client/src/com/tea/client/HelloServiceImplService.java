/**
 * HelloServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.tea.client;

public interface HelloServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getHelloServicePortAddress();

    public HelloService getHelloServicePort() throws javax.xml.rpc.ServiceException;

    public HelloService getHelloServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
