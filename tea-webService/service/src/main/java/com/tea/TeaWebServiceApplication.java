package com.tea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: tea
 * @date: 2020/12/4 2:28
 */
@SpringBootApplication
public class TeaWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeaWebServiceApplication.class, args);
    }

}
