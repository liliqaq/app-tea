package com.tea.service.impl;


import com.tea.service.HelloService;

import javax.jws.WebService;

/**
 * @author: tea
 * @date: 2020/12/4 1:27
 * @description:
 *
 * @WebService: name： 暴露服务名称
 *              targetNamespace：命名空间 一般为接口包名倒写
 *              endpointInterface： 接口地址
 */
@WebService(name = "HelloService", targetNamespace = "http://service.tea.com", endpointInterface = "com.tea.service.HelloService")
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String username) {
        return "hello: \t" + username;
    }
}
