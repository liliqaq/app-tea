package com.tea.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author: tea
 * @date: 2020/12/4 1:24
 * @description:
 *
 * @WebService: name： 暴露服务名称
 *              targetNamespace：命名空间 一般为接口包名倒写
 */
@WebService(name = "HelloService",
        targetNamespace = "http://service.tea.com")
public interface HelloService {

    /**
     * 提供服务的接口
     *
     * @param username 用户名
     * @return
     */
    @WebMethod
    @WebResult(name = "String", targetNamespace = "")
    String hello(@WebParam(name = "username") String username);
}
