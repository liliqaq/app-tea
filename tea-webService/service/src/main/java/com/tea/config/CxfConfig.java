package com.tea.config;

import com.tea.service.HelloService;
import com.tea.service.impl.HelloServiceImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * http://localhost/service/api?wsdl
 *
 * @author: tea
 * @date: 2020/12/4 2:32
 */
@Configuration
public class CxfConfig {

    /**
     * 此处代码会冲掉restController，删掉controller可以正常使用但是webservice失效，需要在application.yml文件中配置
     * cxf.path=/service
     * 这样可以解决springboot与CXF的大部分版本冲突问题
     * @return
     */
   @Bean
   public ServletRegistrationBean servletRegistrationBean() {
       return new ServletRegistrationBean(new CXFServlet(), "/service/*");
   }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public HelloService helloService(){
        return new HelloServiceImpl();
    }

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), helloService());
        endpoint.publish("/api");
        return endpoint;
    }

}
